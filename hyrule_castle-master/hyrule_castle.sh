#!/bin/bash

# ------------- Function ------------
Player.Csv() {
    id=$((1 + $RANDOM % 5))
    while IFS=',' read -r d  name hp mp str int def res spd luck race class rarity; do
	if [[ $id == $d ]]; then
	    Myplayer_name=$name
	    Myplayer_hp=$hp      
	    Myplayer_hpmax=$hp
	    Myplayer_halfhp=$((hp / 2))
	    Myplayer_str=$str
	fi 
    done < players.csv
}

Monster.Csv() {
    id=$((1 + $RANDOM % 12))
    while IFS=',' read -r d name hp mp str int def res spd luck race class rarity; do
	#id=$((1 + $RANDOM % 10))
	if [[ $id == $d ]]; then 
	    Monster_name=$name
	    Monster_hp=$hp
	    Monster_maxhp=$hp
	    Monster_str=$str
	fi
	
    done < enemies.csv
}


Bosses.Csv() {
    id=$((1 + $RANDOM % 7))
    while IFS=',' read -r d name hp mp str int def res spd luck race class rarity; do
	if [[ $id == $d ]]; then
	    Boss_name=$name
	    Boss_hp=$hp
	    Boss_maxhp=$hp
	    Boss_str=$str
	fi
    done < bosses.csv
}

fight() {
   
    while [[ $Monster_hp -gt  0 ]]; do
	echo ""
	echo ""
	echo "|||||||||||| The fight begin |||||||||||||||"
	echo "----------- Choose your action -------------"
	echo "          1                       2         "
	echo "====================="  "==================="
	echo "|||||| Attack |||||||"  "|||||| Heal |||||||"
	echo "====================="  "==================="
	read choix
	if [[ $choix != 1 ]] && [[ $choix != 2 ]]; then
	    echo  "Error ! Please try again !!!!!"
	    echo "----------- Choose your action -------------"
	    echo "          1                       2         "
	    echo "====================="  "==================="
	    echo "|||||| Attack |||||||"  "|||||| Heal |||||||"
	    echo "====================="  "==================="
	    read  choix
	elif [[ $choix == 1 ]]; then
	    Monster_hp=$((Monster_hp - Myplayer_str))
	    echo "You've done $Myplayer_str damages to $Monster_name ."
	    echo ""
	    echo ""
	    echo "||||||||||||||||||||||||||||||||||||||||||||||||||  $Monster_name  |||||||""   $Monster_hp ""/ " "$Monster_maxhp"
	    echo ""
	    echo ""
	    
	elif [[ $choix == 2 ]]; then
	 #   echo $Myplayer_halfhp
	  #  echo $Myplayer_hpmax
	   # echo $Myplayer_hp
	    
	    if [[ $Myplayer_hp -ge $Myplayer_halfhp ]]; then
		Myplayer_hp=$Myplayer_hpmax
		echo ""
		echo " Great move ! you are now full of HP!"
		echo ""
		echo "||||||||||||||||||||||||||||||||||||||||||||||||"" $Myplayer_name ""|||||||" "$Myplayer_hp" " / " "$Myplayer_hpmax"
		read 
	    else
		Myplayer_hp=$((Myplayer_hp + Myplayer_halfhp))
		echo " You used Heal ! "
		echo "|||||||||||||||||||||||||||||||||||||||||||||||| $Myplayer_name |||||||||||| $Myplayer_hp / $Myplayer_hpmax"
		
	    fi
	fi
	if [[ $Monster_hp -gt 0 ]]; then 
	    echo " Now it's  $Monster_name turn"
	    echo ""
	    Myplayer_hp=$((Myplayer_hp - Monster_str))
	    echo "==> $Monster_name attacked and dealt $Monster_str damages to $Myplayer_name !"
	    echo ""
	    echo " -------------------------------------------------------------------------- $Myplayer_name --------- $Myplayer_hp / $Myplayer_hpmax"
	else
	    echo "                                ||||||||||||||||||| $Monster_name is dead. ||||||||||||||||"
	fi
    if [[ $Myplayer_hp -le 0 ]]; then
	echo "----------------------------------------- Game Over -----------------------------------------"
	read ok
	exit 0
    else echo ""
    fi
done
}
#---------        ----------------------------------         --- End Function -----------

#----------- Liste de Variables ------ 
floor=1

Player.Csv
Monster.Csv
Bosses.Csv

#-------------- Code start -----------


echo " "
echo "        ----------------------------------         "
echo "       |   Welcome to the Hyrule Castle   |        "
echo "        ----------------------------------         "
echo " "
read ok
clear
echo "                            Hello Jawa !                              "
echo "    You finally arrived into the Hyrule Castle, congratulations !     "
echo "But now it's time to fight against strong monsters through 10 floors !"
echo " Are you ready for this new challenge ? I hope so, either you'll die. "
echo "                                ...                                   "
read ok
clear
echo "  The main character, the monster and the boss are random !  "
echo " So, your character for the whole castle is $Myplayer_name ! "
echo "     The enemy for the 9 first floors is $Monster_name !     "
echo "    Now it is time to get in the first floor, good luck !    "
read ok
clear

#-------- loop for the 9 first floor --

while [[ $floor != 10 ]]; do	
    echo "|||||||||||||||||||||||| FLOOR "$floor"||||||||||||||"
    Monster_hp=$Monster_maxhp 
   # echo $Myplayer_halfhp
    echo $Myplay
    fight	                                                                                            #Boucle pour réaliser les 9 premiers étages	

    clear
    echo $floor
    if [[ $Monster_hp -le 0 ]]; then
	floor=$((floor + 1))
    fi		       
done			
clear
#-------- loop for the 10 floor
		echo "------------------------------ You entered in the Boss Floor   -----------------------------------------"
echo ""
echo "You will fight against $Boss_name ( Strength : $Boss_str, Life : $Boss_hp )"
echo ""
echo "I am scared! Good Luck"
read ok
clear

while [[ $Boss_hp -gt  0 ]]; do
    echo ""
    echo ""
    echo "----------- Choose your action -------------"
    echo "          1                       2         "
    echo "====================="  "==================="
    echo "|||||| Attack |||||||"  "|||||| Heal |||||||"
    echo "====================="  "==================="
    read choix
    if [[ $choix != 1 ]] && [[ $choix != 2 ]]; then
	echo "error ! Please try again!!!!"
	echo "----------- Choose your action -------------"
	echo "          1                       2         "
	echo "====================="  "==================="
	echo "|||||| Attack |||||||"  "|||||| Heal |||||||"
	echo "====================="  "==================="
	read  choix
    elif [[ $choix == 1 ]]; then
	Boss_hp=$((Boss_hp - Myplayer_str))
	echo "==> $Myplayer_name did $Myplayer_str damages to $Boss_name ."
	echo ""
	echo ""
	echo "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  $Boss_name  |||||||""   $Boss_hp ""/ " "$Boss_maxhp"
	echo ""
	echo ""
	read ok
    elif [[ $choix == 2 ]]; then
	
	if [[ $Myplayer_hp -ge $Myplayer_halfhp ]]; then
	    Myplayer_hp=$Myplayer_hpmax
	    echo ""
	    echo " Now you are full life !"
	    echo ""
	    echo "------------------------------------------------------------------------"" $Myplayer_name ""------------" "$Myplayer_hp" " / " "$Myplayer_hpmax"
	    read ok
	else
	    Myplayer_hp=$((Myplayer_hp + Myplayer_halfhp))
	    echo "==> You healed yourself ! "
	    echo "------------------------------------------------------------------------- $Myplayer_name ------------- $Myplayer_hp / $Myplayer_hpmax"
	    read ok
	fi
    fi
    if [[ $Boss_hp -gt 0 ]]; then
	echo "==> Now $Boss_name can attack"
	echo ""
	Myplayer_hp=$((Myplayer_hp - Boss_str))
	echo "==> $Boss_name attacked and dealt $Boss_str damages to $Myplayer_name !"
	echo ""
	echo " -------------------------------------------------------------------------- $Myplayer_name --------- $Myplayer_hp / $Myplayer_hpmax"
    else
	echo "                                -------------------- $Boss_name is dead. ---------------------"
	echo "                             -------------------------- Congratulations ---------------------------"
    fi
    if [[ $Myplayer_hp -le 0 ]]; then
	echo "----------------------------------------- Game Over -----------------------------------------"
	read ok
	exit 1                                                                                                                                                                                                                              
    fi
done


